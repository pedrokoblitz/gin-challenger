package database

import (
    "gorm.io/driver/sqlite"
    "gorm.io/gorm"
    "log"
    "gin-challenger/models"
)

var DB *gorm.DB

func Connect() {
    db, err := gorm.Open(sqlite.Open(":memory:"), &gorm.Config{})
    if err != nil {
        log.Fatal("Failed to connect to database:", err)
    }

    db.AutoMigrate(&models.Product{}, &models.Purchase{}, &models.User{})
    DB = db
}
