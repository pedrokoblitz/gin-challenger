package models

import "gorm.io/gorm"

type Product struct {
    gorm.Model
    Title       string
    Price       int
    Zipcode     string
    Seller      string
    ThumbnailHd string
    Date        string
}
