package models

import (
    "gorm.io/gorm"
    "github.com/google/uuid"
)

type Purchase struct {
    gorm.Model
    ClientID      uuid.UUID
    ClientName    string
    TotalToPay    int
    CardNumber    string
    PurchaseID    uuid.UUID
    Date          string
}
