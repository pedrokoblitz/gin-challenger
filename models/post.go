package models

import "gorm.io/gorm"

type Post struct {
    gorm.Model
    Title      string
    Author     string
    CreatedAt  int64
    Ups        int
    NumComments int
}