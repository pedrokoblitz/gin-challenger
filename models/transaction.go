package models

import "gorm.io/gorm"

type Transaction struct {
    gorm.Model
    Type      int
    Date      string
    Value     float64
    CPF       string
    Card      string
    Time      string
    Owner     string
    StoreName string
}
