package routes

import (
    "github.com/gin-gonic/gin"
    "gin-challenger/controllers"
    "gin-challenger/middlewares"

)

func SetupRoutes(r *gin.Engine) {

    //
    r.LoadHTMLGlob("views/*")
    r.GET("/", func(c *gin.Context) {
        c.HTML(http.StatusOK, "index.html", nil)
    })
    r.POST("/upload", controllers.UploadFile)

    //
    r.GET("/posts", controllers.GetPosts)
    r.GET("/authors", controllers.GetAuthors)    

    //
    r.GET("/exchange/:amount/:from/:to/:rate", controllers.ConvertCurrency)

    //
    r.POST("/product", middlewares.Auth(), controllers.AddProduct)
    r.GET("/products", controllers.GetProducts)
    r.POST("/buy", middlewares.Auth(), controllers.Buy)
    r.GET("/history", middlewares.Auth(), controllers.GetPurchaseHistory)
    r.GET("/history/:clientId", middlewares.Auth(), controllers.GetPurchaseHistoryByClientID)

}

