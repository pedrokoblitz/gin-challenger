package middlewares

import (
    "github.com/gin-gonic/gin"
    "net/http"
    "strings"
    "starstore/utils"
)

func Auth() gin.HandlerFunc {
    return func(c *gin.Context) {
        token := c.GetHeader("Authorization")
        if token == "" {
            c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header is required"})
            c.Abort()
            return
        }

        token = strings.TrimPrefix(token, "Bearer ")
        if _, err := utils.ValidateToken(token); err != nil {
            c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
            c.Abort()
            return
        }

        c.Next()
    }
}
