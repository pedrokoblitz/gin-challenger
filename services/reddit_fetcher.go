package services

import (
    "encoding/json"
    "log"
    "net/http"
    "reddit-fetcher/models"
    "reddit-fetcher/database"
)

const redditURL = "https://api.reddit.com/r/artificial/hot"

type RedditResponse struct {
    Data struct {
        Children []struct {
            Data struct {
                Title       string `json:"title"`
                Author      string `json:"author"`
                CreatedUtc  int64  `json:"created_utc"`
                Ups         int    `json:"ups"`
                NumComments int    `json:"num_comments"`
            } `json:"data"`
        } `json:"children"`
    } `json:"data"`
}

func FetchAndSavePosts() {
    resp, err := http.Get(redditURL)
    if err != nil {
        log.Println("Error fetching posts:", err)
        return
    }
    defer resp.Body.Close()

    var redditResp RedditResponse
    if err := json.NewDecoder(resp.Body).Decode(&redditResp); err != nil {
        log.Println("Error decoding response:", err)
        return
    }

    for _, postData := range redditResp.Data.Children {
        post := models.Post{
            Title:      postData.Data.Title,
            Author:     postData.Data.Author,
            CreatedAt:  postData.Data.CreatedUtc,
            Ups:        postData.Data.Ups,
            NumComments: postData.Data.NumComments,
        }
        database.DB.Create(&post)
    }
}
