package services

import (
    "github.com/robfig/cron/v3"
    "log"
)

func ScheduleFetch() {
    c := cron.New()
    _, err := c.AddFunc("@daily", FetchAndSavePosts)
    if err != nil {
        log.Fatal("Error scheduling fetch:", err)
    }
    c.Start()
}
