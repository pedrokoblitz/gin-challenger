package services

import (
    "bufio"
    "cnab-parser/models"
    "cnab-parser/database"
    "os"
    "strconv"
    "strings"
)

func ParseFile(filePath string) error {
    file, err := os.Open(filePath)
    if err != nil {
        return err
    }
    defer file.Close()

    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        line := scanner.Text()

        transactionType, _ := strconv.Atoi(line[0:1])
        date := line[1:9]
        value, _ := strconv.ParseFloat(line[9:19], 64)
        value = value / 100.0
        cpf := line[19:30]
        card := line[30:42]
        time := line[42:48]
        owner := strings.TrimSpace(line[48:62])
        storeName := strings.TrimSpace(line[62:81])

        transaction := models.Transaction{
            Type:      transactionType,
            Date:      date,
            Value:     value,
            CPF:       cpf,
            Card:      card,
            Time:      time,
            Owner:     owner,
            StoreName: storeName,
        }

        database.DB.Create(&transaction)
    }

    return scanner.Err()
}
