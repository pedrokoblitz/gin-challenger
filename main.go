package main

import (
    "github.com/gin-gonic/gin"
    "gin-challenger/routes"
    "gin-challenger/database"
    "gin-challenger/services/reddit"    
)

func main() {
    r := gin.Default()
    database.Connect()
    routes.SetupRoutes(r)
    reddit.ScheduleFetch()
    r.Run(":8080")
}
