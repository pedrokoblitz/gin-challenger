package controllers

import (
    "github.com/gin-gonic/gin"
    "net/http"
    "starstore/database"
    "starstore/models"
    "github.com/google/uuid"
)

func Buy(c *gin.Context) {
    var purchase models.Purchase
    if err := c.ShouldBindJSON(&purchase); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }

    purchase.PurchaseID = uuid.New()
    database.DB.Create(&purchase)
    c.JSON(http.StatusOK, purchase)
}

func GetPurchaseHistory(c *gin.Context) {
    var purchases []models.Purchase
    database.DB.Find(&purchases)
    c.JSON(http.StatusOK, purchases)
}

func GetPurchaseHistoryByClientID(c *gin.Context) {
    clientID := c.Param("clientId")
    var purchases []models.Purchase
    database.DB.Where("client_id = ?", clientID).Find(&purchases)
    c.JSON(http.StatusOK, purchases)
}
