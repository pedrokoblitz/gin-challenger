package controllers

import (
    "github.com/gin-gonic/gin"
    "net/http"
    "reddit-fetcher/database"
    "reddit-fetcher/models"
)

func GetPosts(c *gin.Context) {
    var posts []models.Post
    startDate := c.Query("start")
    endDate := c.Query("end")
    orderBy := c.Query("orderBy")

    query := database.DB
    if startDate != "" && endDate != "" {
        query = query.Where("created_at BETWEEN ? AND ?", startDate, endDate)
    }

    if orderBy == "ups" {
        query = query.Order("ups DESC")
    } else if orderBy == "num_comments" {
        query = query.Order("num_comments DESC")
    }

    query.Find(&posts)
    c.JSON(http.StatusOK, posts)
}

func GetAuthors(c *gin.Context) {
    var authors []string
    orderBy := c.Query("orderBy")

    query := database.DB.Model(&models.Post{}).Select("author").Group("author")
    if orderBy == "ups" {
        query = query.Order("SUM(ups) DESC")
    } else if orderBy == "num_comments" {
        query = query.Order("SUM(num_comments) DESC")
    }

    query.Find(&authors)
    c.JSON(http.StatusOK, authors)
}
