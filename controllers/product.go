package controllers

import (
    "github.com/gin-gonic/gin"
    "net/http"
    "starstore/database"
    "starstore/models"
)

func AddProduct(c *gin.Context) {
    var product models.Product
    if err := c.ShouldBindJSON(&product); err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
        return
    }
    database.DB.Create(&product)
    c.JSON(http.StatusOK, product)
}

func GetProducts(c *gin.Context) {
    var products []models.Product
    database.DB.Find(&products)
    c.JSON(http.StatusOK, products)
}
