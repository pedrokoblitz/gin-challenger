package controllers

import (
    "github.com/gin-gonic/gin"
    "gin/services"
    "net/http"
)

func UploadFile(c *gin.Context) {
    file, _ := c.FormFile("file")
    c.SaveUploadedFile(file, file.Filename)
    
    err := services.ParseFile(file.Filename)
    if err != nil {
        c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
        return
    }

    c.JSON(http.StatusOK, gin.H{"message": "File processed successfully"})
}
