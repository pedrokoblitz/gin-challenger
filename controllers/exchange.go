package controllers

import (
    "github.com/gin-gonic/gin"
    "net/http"
    "strconv"
)

func ConvertCurrency(c *gin.Context) {
    amountStr := c.Param("amount")
    from := c.Param("from")
    to := c.Param("to")
    rateStr := c.Param("rate")

    amount, err := strconv.ParseFloat(amountStr, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid amount"})
        return
    }

    rate, err := strconv.ParseFloat(rateStr, 64)
    if err != nil {
        c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid rate"})
        return
    }

    convertedValue := amount * rate
    currencySymbol := getCurrencySymbol(to)

    c.JSON(http.StatusOK, gin.H{
        "valorConvertido": convertedValue,
        "simboloMoeda":    currencySymbol,
    })
}

func getCurrencySymbol(currency string) string {
    switch currency {
    case "USD":
        return "$"
    case "BRL":
        return "R$"
    case "EUR":
        return "€"
    default:
        return ""
    }
}
